ExecuteSoapAction(struct upnphttp * h, const char * action, int n)
{
    char * p;
    char method[2048];
    int i, len, methodlen;

    i = 0;
    p = strchr(action, '#');
    methodlen = strchr(p, '"') - p - 1;

    if(p)
    {   
        p++;
        while(soapMethods[i].methodName)
        {   
            len = strlen(soapMethods[i].methodName);
            if(strncmp(p, soapMethods[i].methodName, len) == 0)
            {
                soapMethods[i].methodImpl(h);
                return;
            }
            i++;
        }

        memset(method, 0, 2048);
        memcpy(method, p, methodlen);
        syslog(LOG_NOTICE, "SoapMethod: Unknown: %s", method);
    }

    SoapError(h, 401, "Invalid Action");
}
